"use strict";

const User = use("App/Models/User");
const { validate } = use("Validator");

class UserController {
  create({ view }) {
    return view.render("user.create");
  }

  async store({ auth, session, request, response }) {
    const data = request.only(["username", "email", "password"]);

    const validation = await validate(data, {
      username: "required",
      email: "required",
      password: "required",
    });

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll();
      return response.redirect("back");
    }

    const user = await User.create(data);

    session.flash({ notification: "User created successfully" });

    return response.redirect("/");
  }
}

module.exports = UserController;
